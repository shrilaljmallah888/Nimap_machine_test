
/**
 * Define your module's express routes here.
 */
const product = require('../controller/product'); // importing User controller.
const { wrapper } = require('../../utils/wrapper'); // importing Error Wrapper.

const express = require('express');

const route = express.Router();

route.post('/', wrapper(product.add));  // api to create data.

route.get('/', wrapper(product.getDetails)); // api to read data.

route.get('/:_id', wrapper(product.getDetailById)); // api to read data by id.

route.put('/:_id', wrapper(product.updateData)); // api to update data by id.

route.delete('/:_id', wrapper(product.removeData)); // api to remove data by id.

module.exports = route;
