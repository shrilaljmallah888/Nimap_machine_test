
// importing all required modules

const product = require('../route/product');
const category = require('../route/category');

const express = require('express');

// Defining express route.  

const route = express.Router();

route.use('/product', product);
route.use('/category', category);


module.exports = route;
