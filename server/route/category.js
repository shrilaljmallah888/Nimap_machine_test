
/**
 * Define your module's express routes here.
 */
const category = require('../controller/category'); // importing User controller.
const { wrapper } = require('../../utils/wrapper'); // importing Error Wrapper.

const express = require('express');

const route = express.Router();

route.post('/', wrapper(category.add));  // api to create data.

route.get('/', wrapper(category.getDetails)); // api to read data.

route.get('/:_id', wrapper(category.getDetailById)); // api to read data by id.

route.put('/:_id', wrapper(category.updateData)); // api to update data by id.

route.delete('/:_id', wrapper(category.removeData)); // api to remove data by id.

module.exports = route;
