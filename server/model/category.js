/**
 * category Schema
 */

// importing all required modules

const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
    }
});

const category = mongoose.model('category', categorySchema);

exports.add = data => category.create(data); // function to create data.

exports.get = () => category.find(); // function to read data.

exports.getById = _id => category.findById(_id); // function to read data by id.

exports.updateById = (_id, data) => category.findByIdAndUpdate({ _id }
    , data, { new: true }); // function to update data by id.

exports.deleteData = _id => category.findByIdAndRemove({ _id }); // remove data by id.

// Export the Mongoose model

exports.model = categorySchema;
