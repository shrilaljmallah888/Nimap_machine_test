/**
 * product Schema
 */

// importing all required modules

const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    categoryId : {
        type : mongoose.Schema.Types.ObjectId, ref : 'category'
    }

});

const product = mongoose.model('product', productSchema);

exports.add = data => product.create(data); // function to create data.

 exports.get = (limitvalue,skipvalue) => product.find().limit(parseInt(limitvalue)).skip(parseInt(skipvalue)).populate([{path : 'categoryId'}]) // function to read data.

exports.getById = _id => product.findById(_id); // function to read data by id.

exports.updateById = (_id, data) => product.findByIdAndUpdate({ _id }
    , data, { new: true }); // function to update data by id.

exports.deleteData = _id => product.findByIdAndRemove({ _id }); // remove data by id.

// Export the Mongoose model

exports.model = productSchema;
