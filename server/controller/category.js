/**
 * Define controller function here.
 */

const category = require('../model/category'); // importing category schema.

// Create category master.
exports.add = async (req, res) => {
    let categoryCreated = await category.add(req.body);
    if (categoryCreated) {
        res.status(201).send({ message: 'data created' })
    } else {
        res.status(422).send({ message: 'bad created' })
    }
};

// Read category master with pagination.
exports.getDetails = async (req, res) => {
    let categoryData = await category.get();
    if (categoryData) {
        res.status(200).send({ message: 'data found', result: categoryData })
    } else {
        res.status(404).send({ message: 'no data found' })
    }
};

// Read category master by id.
exports.getDetailById = async (req, res) => {
    let categoryData = await category.getById(req.params._id);
    if (categoryData) {
        res.status(200).send({ message: 'data found', result: categoryData })
    } else {
        res.status(404).send({ message: 'no data found' })
    }
};

// Update category master by id.
exports.updateData = async (req, res) => {
    let categoryUpdated = await category.updateById(req.params._id, req.body);
    if (categoryUpdated) {
        res.status(200).send({ message: 'category updated' })
    } else {
        res.status(404).send({ message: 'no data found' })
    }
};

// Remove category master by id.
exports.removeData = async (req, res) => {
    let categoryDeleted = await category.deleteData(req.params._id);
    if (categoryDeleted) {
        res.status(200).send({ message: 'data deleted' })
    } else {
        res.status(404).send({ message: 'no data found' })
    }
};
