/**
 * Define controller function here.
 */

const product = require('../model/product'); // importing product schema.

// Create product master.
exports.add = async (req, res) => {
    let productCreated = await product.add(req.body);
    if (productCreated) {
        res.status(201).send({ message: 'data created' })
    } else {
        res.status(422).send({ message: 'bad created' })
    }
};

// Read product master with pagination.
exports.getDetails = async (req, res) => {
const limitvalue = req.query.limit ||2;
const skipvalue = req.query.skip ||0;
 let productData = await product.get(limitvalue,skipvalue)
    if (productData) {
        res.status(200).send({ message: 'data found', result: productData })
    } else {
        res.status(404).send({ message: 'no data found' })
    }
};

// Read product master by id.
exports.getDetailById = async (req, res) => {
    let productData = await product.getById(req.params._id);
    if (productData) {
        res.status(200).send({ message: 'data found', result: productData })
    } else {
        res.status(404).send({ message: 'no data found' })
    }
};

// Update product master by id.
exports.updateData = async (req, res) => {
    let productUpdated = await product.updateById(req.params._id, req.body);
    if (productUpdated) {
        res.status(200).send({ message: 'product updated' })
    } else {
        res.status(404).send({ message: 'no data found' })
    }
};

// Remove product master by id.
exports.removeData = async (req, res) => {
    let productDeleted = await product.deleteData(req.params._id);
    if (productDeleted) {
        res.status(200).send({ message: 'data deleted' })
    } else {
        res.status(404).send({ message: 'no data found' })
    }
};
